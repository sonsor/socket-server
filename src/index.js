const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const start = require('./server').start;
const logger = require('./config/logger');

if (cluster.isMaster) {
    logger.info(`Master ${process.pid} is running`);

    // Fork workers.
    for (let i = 0; i < 1; i++) {
        let worket = cluster.fork();
        worket.on('exit', (code, signal) => {

            if (signal) {
                logger.info(`worker was killed by signal: ${signal}`);
            } else if (code !== 0) {
                logger.info(`Starting new worker after previous one exited`);
                cluster.fork();
            }

        })
    }

    cluster.on('exit', (worker, code, signal) => {
        logger.warn(`worker ${worker.process.pid} died`);
        cluster.fork();
    });
} else {
    start();
}
