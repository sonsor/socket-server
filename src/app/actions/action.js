const logger = require('../../config/logger');

/**
 *
 */
class Action {

    /**
     *
     * @param client
     * @param headers
     */
    constructor (client, headers) {
        this._client = client;
        this._headers = headers;

        this._data = null;
        this._error = null;
        this._status = null;

        this._type = '';
    }

    /**
     *
     * @returns {*}
     */
    get client() {
        return this._client;
    }

    /**
     *
     * @param value
     */
    set client(value) {
        this._client = value;
    }

    /**
     *
     * @returns {*}
     */
    get headers() {
        return this._headers;
    }

    /**
     *
     * @param value
     */
    set headers(value) {
        this._headers = value;
    }

    /**
     *
     * @returns {null}
     */
    get data() {
        return this._data;
    }

    /**
     *
     * @param value
     */
    set data(value) {
        this._data = value;
    }

    /**
     *
     * @returns {null}
     */
    get error() {
        return this._error;
    }

    /**
     *
     * @param value
     */
    set error(value) {
        this._error = value;
    }

    /**
     *
     * @returns {null}
     */
    get status() {
        return this._status;
    }

    /**
     *
     * @param value
     */
    set status(value) {
        this._status = value;
    }

    /**
     *
     * @returns {string}
     */
    get type() {
        return this._type;
    }

    /**
     *
     * @param value
     */
    set type(value) {
        this._type = value;
    }

    /**
     *
     * @returns {Action}
     */
    get send() {
        return this;
    }

    /**
     *
     * @returns {Action}
     */
    get to() {
        return this;
    }

    /**
     *
     * @returns {Action}
     */
    get me() {
        this.type = 'me';
        this.trigger();
        return this;
    }

    /**
     *
     * @returns {Action}
     */
    get room() {
        this.type = 'room';
        this.trigger();
        return this;
    }

    /**
     *
     * @returns {Action}
     */
    get all() {
        this.type = 'all';
        this.trigger();
        return this;
    }

    get message() {
        let response = {};
        response.headers = {
            ...this.headers,
            status: this.status
        };

        response.body = this.data;
        return JSON.stringify(response);
    }

    /**
     *
     * @param code
     * @returns {Action}
     */
    status(code) {
        this.status = code;
        return this;
    }

    /**
     *
     * @param data
     * @returns {Action}
     */
    response(data) {
        this.data = data;
        return this;
    }

    /**
     *
     */
    trigger() {

        /**
         *
         * @type {null}
         */
        let to = null;

        switch (this.type) {
            case 'me':
                to = this.headers.tabId;
                break;
            case 'room':
                to = this.headers.windowId;
                break;
            case 'all':
                to = 'web';
                break;
        }


        logger.info("to : " + to);

        if (to) {
            this.client.to(to).emit('response', this.message);
        }
    }
}

module.exports = Action;