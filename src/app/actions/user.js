const Action = require('./action');
const logger = require('../../config/logger');

class User extends Action {

    async get(data) {

        try {
            return this.response(data).status(200).send.to.room;
        } catch (e) {
            return this.response(e).status(500).send.to.me;
        }
    }

    static call(type, payload, client, headers) {
        let instance = new User(client, headers);

        switch (type) {
            case 'USER_GET':
                return instance.get(payload);
              break;
        }

        return false;
    }
}

module.exports = User;
