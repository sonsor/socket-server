const actions = require('./actions');
const logger = require('../config/logger');
const db = require('../config/db');

const dispatch = (
    type,
    payload,
    client,
    headers
) => {

    let result = null;
     if (!type) {
         return false;
     }

    const profiler = logger.startTimer();

    logger.info('connecting to database');
    db.connect();
    logger.info('connection established');

     for (let action in actions) {

         result = actions[action].call(
             type,
             payload,
             client,
             headers
         );

         if (result === false) {
             logger.info(`Action ${type} found`);
             break;
         }
     }

     if (!result) {
         profiler.done({ message: `Action ${type} not found`});
         client.to(headers.tabId).emit('error', JSON.stringify({
            headers: {
                ...headers,
                status: 404
            },
             body: {
                 message: 'the action you desire to call not found'
             }
         }));

         return;
     }

     result.then(() => {
         //logger.info(`Action ${type} completed`);
         profiler.done({ message: `Action ${type} completed`});
         db.disconnect();
     }).catch(err => {
         profiler.done({ message: `Action ${type} throw error`});
         logger.error(err);
         db.disconnect();
     });
}

module.exports = dispatch;