const Socket = require('socket.io');
//const config = require('./config');
const logger = require('./config/logger');
const http = require('http');
const dispatch = require('./app/dispatcher');

class Server extends Socket {

    constructor(app) {
        super(9000);
        logger.info('Socket server started at http://localhost:9000');
        this.on('connection', this.onConnection.bind(this));
    }

    static start() {
        return new Server(http.createServer(Server.handle));
    }

    onConnection(socket) {
        logger.info('client connected');

        this.client = socket;

        socket.on('enter', this.onEnter.bind(this));
        socket.on('data', this.onData.bind(this));

        socket.on('disconnect', this.onDisconnect.bind(this));
    }

    onEnter(data) {
        this.client.join('web');
        this.client.join(data.headers.windowId);
        this.client.join(data.headers.tabId);

        this.client.emit('joined', {
            data: 'successfully joined rooms'
        })
    }

    onData(message) {
        logger.info(message);
        const {
            headers,
            body: {
                action,
                data
            }
        } = JSON.parse(message.toString());

        dispatch(
            action,
            data,
            this,
            headers
        );
    }

    onDisconnect() {
        logger.info('client disconnected');
    }
}

module.exports = Server;