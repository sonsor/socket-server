const path = require('path');
require('dotenv').config({
    path: path.join(__dirname, '../../.env'),
    sample: path.join(__dirname, '../../.env.example'),
});

module.exports = {
    env: process.env.NODE_ENV,
    socket: {
        port: process.env.SOCKET_PORT,
        namespace: process.env.SOCKET_NAMESPACE,
        room: process.env.SOCKET_DEFAULT_ROOM
    },
    db: {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        user: process.env.DB_USER,
        pass: process.env.DB_PASS,
        name: process.env.DB_NAME,
        driver: process.env.DB_DRIVER
    }
}