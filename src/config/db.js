const mongoose = require('mongoose');
const logger = require('./logger');
const { db, env } = require('./vars');

mongoose.Promise = Promise;

// Exit application on error
mongoose.connection.on('error', (err) => {
    logger.error(`MongoDB connection error: ${err}`);
    process.exit(-1);
});

// print mongoose logs in dev env
if (env === 'development') {
    mongoose.set('debug', true);
}

/**
 * Connect to mongo db
 *
 * @returns {object} Mongoose connection
 * @public
 */
exports.connect = () => {
    let uri = 'mongodb://';

    if (db.user && db.pass) {
        uri += db.user + ':' + db.pass + '@';
    }

    uri += db.host;
    uri += ':' + (db.port ? db.port: 27017);
    uri += '/' + db.name;

    mongoose.connect(uri, {
        keepAlive: 1,
        useNewUrlParser: true,
    });
    return mongoose.connection;
};

exports.disconnect = () => {
    mongoose.disconnect();
    logger.info('database disconnected');
}


